# Simple Backgammon Gym

# Table of Contents
1. [Quick Start](#quick-start)
2. [Introduction](#introduction)
3. [Game Engine](#game-engine)
4. [Feature Space](#feature-space)
5. [Configuring Your Agent](#configuring-your-agent)
6. [Technology Used](#technology-used)

## Quick Start
To run the project make sure to install the related python packages with pip. Then run the code. This should be as simple as...

```
pip install -r requirements.txt
python3 main.py
```

## Introduction
Backgammon Simple Gym is a reinforcement learning environment inspired by Teseuvos' TD-gammon game engine. It is designed to simulate the simplest form of backgammon, focusing on the bearoff phase of the game. The gym features a randomized starting board position, challenging agents to bear off their 15 checkers before their opponent.

## Game Engine
The game engine is responsible for generating the board, moving checkers, and determining the winner. It outputs a set of legal moves represented by board states, along with the identification of a winning player. The primary objective of this RL environment is to train temporal difference agents to select optimal moves from a range of possibilities based on the initial board state.

## Feature Space
The board state consists of 16 elements:
- **White Player's Board Configuration (Elements 0-5)**: Describes the positions of WHITE player's checkers during bearoff. Index 0 represents the furthest point from bearing off, and 5 the closest. Each element indicates the number of checkers at that point.
- **Black Player's Board Configuration (Elements 6-11)**: Structured similarly to the WHITE player's board configuration.
- **Checkers Off the Board (Elements 12-13)**: Element 12 for WHITE and 13 for BLACK, indicating how many checkers each player has borne off.
- **Current Player's Turn (Elements 14-15)**: Element 14 for WHITE's turn and 15 for BLACK's turn.

## Configuring Your Agent

### Example: Random Agent
`main.py` includes an example of a random agent, illustrating the general method of interacting with the game and integrating a reinforcement learning agent for training or competition.

### Initializing the Board
- Initialize a board using the constructor.
- Iterate through legal moves by accessing the `moves` attribute.

### Agent's Responsibility
- Provide a scalar value between 0 and 1 for each move using the `score_move` function, indicating its suitability for the WHITE player.
- After each turn, call `get_winner` to determine if the player has won.

## Technology Used
- **Python**: The primary language used for developing the game engine and scripting the reinforcement learning environment.

**Note**: This environment serves as a tool for studying and applying reinforcement learning strategies in a controlled setting.
